import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

L = 2.
n = 100
dx = L / n
x = np.linspace(0.5 * dx, L - 0.5 * dx, n)


def plot2d(grid):
    x = np.linspace(0, L, n + 1, endpoint=True)
    y = np.linspace(0, L, n + 1, endpoint=True)

    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title(r'$\rho$')
    ax[0, 1].set_title(r'$p$')
    ax[1, 0].set_title(r'$u$')
    ax[1, 1].set_title(r'$v$')

    rho = ax[0, 0].pcolormesh(x, y, grid[0, 0], vmin=grid[:, 0].min(), vmax=grid[:, 0].max())
    p = ax[0, 1].pcolormesh(x, y, grid[0, 3], vmin=grid[:, 3].min(), vmax=grid[:, 3].max())

    u = ax[1, 0].pcolormesh(x, y, grid[0, 1], vmin=grid[:, 1].min(), vmax=grid[:, 1].max())
    v = ax[1, 1].pcolormesh(x, y, grid[0, 2], vmin=grid[:, 2].min(), vmax=grid[:, 2].max())

    def update(i):
        rho.set_array(grid[i, 0])
        p.set_array(grid[i, 3])
        u.set_array(grid[i, 1])
        v.set_array(grid[i, 2])

    animate = animation.FuncAnimation(fig, update, interval=20, repeat=True, frames=size)

    plt.show()


def plot1d(grid, rho_a, u_a, p_a):
    x = np.linspace(0.5 * dx, L - 0.5 * dx, n, endpoint=True) * np.sqrt(2)

    fig, ax = plt.subplots(2, 2)
    rho, = ax[0, 0].plot(x, grid[0, 0].diagonal(), marker='o', linestyle='none', label='numeric')
    p, = ax[0, 1].plot(x, grid[0, 3].diagonal(), marker='o', linestyle='none', label='numeric')
    rho_al, = ax[0, 0].plot(x, rho_a[0], label='analytical')
    p_al, = ax[0, 1].plot(x, p_a[0], label='analytical')

    ax[0, 0].set_ylim(grid[:, 0].min(), 1.1 * grid[:, 0].max())
    ax[0, 1].set_ylim(grid[:, 3].min(), 1.1 * grid[:, 3].max())

    u, = ax[1, 0].plot(x, grid[0, 1].diagonal(), marker='o', linestyle='none', label='numeric')
    v, = ax[1, 1].plot(x, grid[0, 2].diagonal(), marker='o', linestyle='none', label='numeric')
    u_al, = ax[1, 0].plot(x, u_a[0] / np.sqrt(2), label='analytical')
    v_al, = ax[1, 1].plot(x, u_a[0] / np.sqrt(2), label='analytical')

    ax[1, 0].set_ylim(grid[:, 1].min(), 1.1 * grid[:, 1].max())
    ax[1, 1].set_ylim(grid[:, 2].min(), 1.1 * grid[:, 2].max())

    ax[0, 0].set_title(r'$\rho$')
    ax[0, 1].set_title(r'$p$')
    ax[1, 0].set_title(r'$u$')
    ax[1, 1].set_title(r'$v$')

    for i in range(2):
        for j in range(2):
            ax[i, j].grid()
            ax[i, j].legend()

    def update(i):
        rho.set_ydata(grid[i, 0].diagonal())
        p.set_ydata(grid[i, 3].diagonal())
        u.set_ydata(grid[i, 1].diagonal())
        v.set_ydata(grid[i, 2].diagonal())

        rho_al.set_ydata(rho_a[i])
        p_al.set_ydata(p_a[i])
        u_al.set_ydata(u_a[i] / np.sqrt(2))
        v_al.set_ydata(u_a[i] / np.sqrt(2))

    update(150)
    # animate = animation.FuncAnimation(fig, update, interval=20, repeat=True, frames=size)

    plt.show()


for name in ['godunov', 'hllc']:
    path = '../res/sod_2d_{}/'.format(name)
    size = len(os.listdir(path)) // 4
    grid = np.empty((size, 4, n, n))
    for i in range(size):
        grid[i, 0] = np.fromfile(path + 'rho{}.raw'.format(i), dtype=np.double).reshape(n, n)
        grid[i, 1] = np.fromfile(path + 'u{}.raw'.format(i), dtype=np.double).reshape(n, n)
        grid[i, 2] = np.fromfile(path + 'v{}.raw'.format(i), dtype=np.double).reshape(n, n)
        grid[i, 3] = np.fromfile(path + 'p{}.raw'.format(i), dtype=np.double).reshape(n, n)

    rho_a = np.loadtxt(path + 'rho_a')
    u_a = np.loadtxt(path + 'u_a')
    p_a = np.loadtxt(path + 'p_a')

    plot1d(grid, rho_a, u_a, p_a)
    plot2d(grid)
