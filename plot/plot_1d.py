import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


L = 1.
n = 100
dx = L / n
x = np.linspace(dx, L - dx, n)

for name in ['godunov', 'hll', 'hllc', 'ausm']:
    for i in range(1, 6):
        if name == 'ausm' and i == 3:
            continue
        path = '../res/' + name + '_' + str(i) + '/'

        rho = np.loadtxt(path + 'rho')
        u = np.loadtxt(path + 'u')
        p = np.loadtxt(path + 'p')

        rho_a = np.loadtxt(path + 'rho_a')
        u_a = np.loadtxt(path + 'u_a')
        p_a = np.loadtxt(path + 'p_a')


        fig, ax = plt.subplots(1, 3)

        fig.set_size_inches((20, 7))

        fig.suptitle(name)
        ax[0].set_title(r'$\rho$')
        ax[1].set_title(r'$u$')
        ax[2].set_title(r'$p$')

        line0, = ax[0].plot(x, rho[0])
        line1, = ax[1].plot(x, u[0])
        line2, = ax[2].plot(x, p[0])

        line0a, = ax[0].plot(x, rho_a[0])
        line1a, = ax[1].plot(x, u_a[0])
        line2a, = ax[2].plot(x, p_a[0])

        ax[0].set_xlabel(r'$x$')
        ax[1].set_xlabel(r'$x$')
        ax[2].set_xlabel(r'$x$')

        ax[0].set_ylim([- 0.1 * rho.max(), 1.1 * rho.max()])
        ax[1].set_ylim([1.1 * u.min() if u.min() < 0 else -0.1 * u.max(), 1.1 * u.max()])
        ax[2].set_ylim([-0.1 * p.max(), 1.1 * p.max()])

        def animationFunction(i) :
            line0.set_ydata(rho[i])
            line1.set_ydata(u[i])
            line2.set_ydata(p[i])

            line0a.set_ydata(rho_a[i])
            line1a.set_ydata(u_a[i])
            line2a.set_ydata(p_a[i])

        anim = animation.FuncAnimation(fig, animationFunction, interval=50, repeat=True, frames=rho.shape[0])
        # anim.save(name + '_' + str(i) + '.gif')

        plt.show()

        t = np.loadtxt(path + 't')
        rho_e = np.abs(rho - rho_a)
        u_e = np.abs(u - u_a)
        p_e = np.abs(p - p_a)

        fig, ax = plt.subplots(1, 3)

        fig.set_size_inches((20, 7))

        fig.suptitle(name)
        ax[0].set_title(r'$\Delta \rho$')
        ax[1].set_title(r'$\Delta u$')
        ax[2].set_title(r'$\Delta p$')

        ax[0].set_xlabel(r'$t$')
        ax[1].set_xlabel(r'$t$')
        ax[2].set_xlabel(r'$t$')

        ax[0].plot(t, rho_e.max(axis=1))
        ax[1].plot(t, u_e.max(axis=1))
        ax[2].plot(t, p_e.max(axis=1))

        # fig.savefig(name + '_' + str(i) +'_e.png')

        plt.show()
