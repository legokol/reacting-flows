import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

L = 2.
n = 101
dx = L / n
x = np.linspace(0.5 * dx, L - 0.5 * dx, n)
gamma = 1.4


def plot2d(grid, name):
    x = np.linspace(0, L, n + 1, endpoint=True)
    y = np.linspace(0, L, n + 1, endpoint=True)

    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title(r'$\rho$')
    ax[0, 1].set_title(r'$p$')
    ax[1, 0].set_title(r'$u$')
    ax[1, 1].set_title(r'$v$')

    rho = ax[0, 0].pcolormesh(x, y, grid[0, 0], vmin=grid[:, 0].min(), vmax=grid[:, 0].max())
    p = ax[0, 1].pcolormesh(x, y, grid[0, 3], vmin=grid[:, 3].min(), vmax=grid[:, 3].max())

    u = ax[1, 0].pcolormesh(x, y, grid[0, 1], vmin=grid[:, 1].min(), vmax=grid[:, 1].max())
    v = ax[1, 1].pcolormesh(x, y, grid[0, 2], vmin=grid[:, 2].min(), vmax=grid[:, 2].max())

    def update(i):
        rho.set_array(grid[i, 0])
        p.set_array(grid[i, 3])
        u.set_array(grid[i, 1])
        v.set_array(grid[i, 2])

    fig.set_size_inches(8, 8)
    animate = animation.FuncAnimation(fig, update, interval=50, repeat=True, frames=size)
    # animate.save('{}_2d.gif'.format(name))

    plt.show()


def plot1d(grid, name):
    x = np.linspace(0.5 * dx, L - 0.5 * dx, n, endpoint=True)

    fig, ax = plt.subplots(2, 2)
    rho, = ax[0, 0].plot(x, grid[0, 0, n // 2, :])
    p, = ax[0, 1].plot(x, grid[0, 3, n // 2, :])
    ax[0, 0].set_ylim(grid[:, 0].min(), 1.1 * grid[:, 0].max())
    ax[0, 1].set_ylim(grid[:, 3].min(), 1.1 * grid[:, 3].max())

    u, = ax[1, 0].plot(x, grid[0, 1, n // 2, :])
    v, = ax[1, 1].plot(x, grid[0, 2, n // 2, :])
    ax[1, 0].set_ylim(grid[:, 1].min(), 1.1 * grid[:, 1].max())
    ax[1, 1].set_ylim(grid[:, 2].min(), 1.1 * grid[:, 2].max())

    def update(i):
        rho.set_ydata(grid[i, 0, n // 2, :])
        p.set_ydata(grid[i, 3, n // 2, :])
        u.set_ydata(grid[i, 1, n // 2, :])
        v.set_ydata(grid[i, 2, n // 2, :])

    fig.set_size_inches(8, 8)
    animate = animation.FuncAnimation(fig, update, interval=50, repeat=True, frames=size)
    # animate.save('{}_1d.gif'.format(name))

    plt.show()


for name in ['godunov', 'hllc']:
    path = '../res/explosion_{}/'.format(name)
    size = len(os.listdir(path)) // 4
    grid = np.empty((size, 5, n, n))
    for i in range(size):
        grid[i, 0] = np.fromfile(path + 'rho{}.raw'.format(i), dtype=np.double).reshape(n, n)
        grid[i, 1] = np.fromfile(path + 'u{}.raw'.format(i), dtype=np.double).reshape(n, n)
        grid[i, 2] = np.fromfile(path + 'v{}.raw'.format(i), dtype=np.double).reshape(n, n)
        grid[i, 3] = np.fromfile(path + 'p{}.raw'.format(i), dtype=np.double).reshape(n, n)

    grid[:, 4] = 0.5 * (grid[:, 0] * (grid[:, 1] ** 2 + grid[:, 2] ** 2)) + grid[:, 3] / (gamma - 1)
    plot1d(grid, name)
    plot2d(grid, name)
    x = np.linspace(0.5 * dx, L - 0.5 * dx, n, endpoint=True)

"""
pathG = '../res/explosion_{}/'.format('godunov')
pathH = '../res/explosion_{}/'.format('hllc')
size = len(os.listdir(path)) // 4
godunov = np.empty((size, 5, n, n))
hllc = np.empty((size, 5, n, n))
for i in range(size):
    godunov[i, 0] = np.fromfile(pathG + 'rho{}.raw'.format(i), dtype=np.double).reshape(n, n)
    godunov[i, 1] = np.fromfile(pathG + 'u{}.raw'.format(i), dtype=np.double).reshape(n, n)
    godunov[i, 2] = np.fromfile(pathG + 'v{}.raw'.format(i), dtype=np.double).reshape(n, n)
    godunov[i, 3] = np.fromfile(pathG + 'p{}.raw'.format(i), dtype=np.double).reshape(n, n)

    hllc[i, 0] = np.fromfile(pathH + 'rho{}.raw'.format(i), dtype=np.double).reshape(n, n)
    hllc[i, 1] = np.fromfile(pathH + 'u{}.raw'.format(i), dtype=np.double).reshape(n, n)
    hllc[i, 2] = np.fromfile(pathH + 'v{}.raw'.format(i), dtype=np.double).reshape(n, n)
    hllc[i, 3] = np.fromfile(pathH + 'p{}.raw'.format(i), dtype=np.double).reshape(n, n)

godunov[:, 4] = 0.5 * godunov[:, 0] * (np.power(godunov[:, 1], 2) + np.power(godunov[:, 2], 2)) + godunov[:, 3] / (
        gamma - 1)
hllc[:, 4] = 0.5 * hllc[:, 0] * (np.power(hllc[:, 1], 2) + np.power(hllc[:, 2], 2)) + hllc[:, 3] / (gamma - 1)

fig, ax = plt.subplots(2, 2)
ax[0, 0].plot(x, godunov[35, 0, n // 2, :], label='godunov')
ax[1, 0].plot(x, godunov[35, 3, n // 2, :], label='godunov')

ax[0, 0].set_ylim(godunov[:, 0].min(), godunov[:, 0].max())
ax[1, 0].set_ylim(godunov[:, 3].min(), godunov[:, 3].max())

ax[0, 1].plot(x, godunov[35, 1, n // 2, :], label='godunov')
ax[0, 1].set_ylim(0, godunov[:, 1].max())

ax[0, 0].plot(x, hllc[35, 0, n // 2, :], label='hllc')
ax[1, 0].plot(x, hllc[35, 3, n // 2, :], label='hllc')
ax[0, 1].plot(x, hllc[35, 1, n // 2, :], label='hllc')

for j in range(2):
    for k in range(2):
        ax[j, k].set_xlim(1, 2)
        ax[j, k].grid()
        ax[j, k].legend()

ax[0, 0].set_title(r'$\rho$')
ax[1, 0].set_title(r'$p$')
ax[0, 1].set_title(r'$u$')

plt.show()
"""
