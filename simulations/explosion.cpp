#include <filesystem>
#include <fstream>
#include <source_location>

#include <riemann_solvers/Solve.hpp>
#include <riemann_solvers/flux/Godunov.hpp>
#include <riemann_solvers/flux/HLL.hpp>
#include <string>

int main() {
    using namespace riemann_solvers;

    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("simulations");
    const auto resPath         = std::filesystem::path{filePath.substr(0, pos)}.make_preferred() / "res";

    const double L      = 2.;
    const std::size_t n = 101;
    const double dx     = L / static_cast<double>(n);

    const double CFL     = 0.9;
    const double maxTime = 0.7f;

    const double gamma = 1.4;

    const double rho0 = 1;
    const double p0   = 1;
    const double rho1 = 0.125;
    const double p1   = 0.1;

    const double r = 0.4;

    std::vector<vec4> initial(n * n);
    for (std::size_t j = 0; j < n; ++j) {
        const double y = dx * (0.5 + static_cast<double>(j)) - 1;
        for (std::size_t i = 0; i < n; ++i) {
            const double x = dx * (0.5 + static_cast<double>(i)) - 1;
            if (x * x + y * y > r * r) {
                initial[i + j * n] = vec4{rho1, 0, 0, p1};
            } else {
                initial[i + j * n] = vec4{rho0, 0, 0, p0};
            }
        }
    }

    const auto sim = [&](const std::string& name, auto flux) {
        const auto path = resPath / name;
        const Parameters parameters{.path = path, .CFL = CFL, .T = maxTime, .dx = dx, .gamma = gamma};
        const auto& [time, _] = solve(initial, n, n, parameters, flux);
        std::ofstream writer(path / "time");
        std::ranges::copy(time, std::ostream_iterator<double>{writer, "\n"});
    };

    sim("explosion_godunov", godunovFlux2D);
    sim("explosion_hllc", HLLC2D);

    return 0;
}