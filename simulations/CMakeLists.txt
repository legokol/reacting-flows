file(GLOB_RECURSE SOURCES *.cpp)

foreach (SOURCE IN LISTS SOURCES)
    get_filename_component(FILENAME ${SOURCE} NAME_WE)
    set(EXECUTABLE_NAME ${FILENAME})
    add_executable(${EXECUTABLE_NAME} ${SOURCE})
    target_link_libraries(${EXECUTABLE_NAME} PRIVATE ${PROJECT_NAME})
    target_compile_options(${EXECUTABLE_NAME} PRIVATE $<$<CXX_COMPILER_ID:GNU,CLANG>:-Werror -Wall -Wextra -pedantic -Wshadow -Wconversion -Wsign-conversion>
            $<$<CXX_COMPILER_ID:MSVC>:/W4 /WX>)
endforeach ()
