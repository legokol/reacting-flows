#include <filesystem>
#include <fstream>
#include <source_location>
#include <string>

#include <riemann_solvers/Solve.hpp>
#include <riemann_solvers/flux/Godunov.hpp>
#include <riemann_solvers/flux/HLL.hpp>

#include "AnalyticalSolution.hpp"

int main() {
    using namespace riemann_solvers;

    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("simulations");
    const auto resPath         = std::filesystem::path{filePath.substr(0, pos)}.make_preferred() / "res";

    const double L      = 2.;
    const std::size_t n = 100;
    const double dx     = L / static_cast<double>(n);

    const double CFL = 0.4;

    const double gamma = 1.4;

    const double maxTime = 1.;
    const vec4 left      = {1.0, 0.75, 0.75, 1.0};
    const vec4 right     = {0.125, 0.0, 0.0, 0.1};

    std::vector<vec4> initial(n * n);
    for (std::size_t j = 0; j < n; ++j) {
        for (std::size_t i = 0; i < n - 1 - j; ++i) {
            initial[i + j * n] = left;
        }
        for (std::size_t i = n - 1 - j; i < n; ++i) {
            initial[i + j * n] = right;
        }
    }

    const auto sim = [&](const std::string& name, auto flux) {
        const auto path = resPath / name;
        const Parameters parameters{.path = path, .CFL = CFL, .T = maxTime, .dx = dx, .gamma = gamma};
        const auto& [time, _] = solve(initial, n, n, parameters, flux);
        AnalyticalSolution analytical(0.5 * L * sqrt(2), {left(0), 0.75 * sqrt(2), left(3)}, {right(0), 0., right(3)},
                                      gamma);

        std::ofstream rhoWriter(path / "rho_a");
        std::ofstream uWriter(path / "u_a");
        std::ofstream pWriter(path / "p_a");
        for (auto t : time) {
            for (std::size_t j = 0; j < n; ++j) {
                const double x = (static_cast<double>(j) + 0.5) * dx * sqrt(2);
                const vec3 u   = analytical(t, x);
                rhoWriter << u(0) << ' ';
                uWriter << u(1) << ' ';
                pWriter << u(2) << ' ';
            }
            rhoWriter << '\n';
            uWriter << '\n';
            pWriter << '\n';
        }
    };

    sim("sod_2d_godunov", godunovFlux2D);
    sim("sod_2d_hllc", HLLC2D);

    return 0;
}