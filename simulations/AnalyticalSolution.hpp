#pragma once

#include <cmath>
#include <utility>

#include "riemann_solvers/Utils.hpp"

namespace riemann_solvers {

namespace details {

class PressureFunction {
private:
    double rho_;
    double p_;
    double gamma_;
    double a_;
    double A_;
    double B_;

public:
    PressureFunction(double rho, double p, double gamma)
        : rho_{rho},
          p_{p},
          gamma_{gamma},
          a_{std::sqrt(gamma * p / rho)},
          A_{2. / ((gamma + 1.) * rho)},
          B_{(gamma - 1.) / (gamma + 1.) * p} {}

    double operator()(double p) const {
        if (p > p_) {
            return (p - p_) * std::sqrt(A_ / (p + B_));
        } else {
            return 2. * a_ / (gamma_ - 1.) * (std::pow(p / p_, 0.5 * (gamma_ - 1.) / gamma_) - 1.);
        }
    }

    double df(double p) const {
        if (p > p_) {
            return std::sqrt(A_ / (p + B_)) * (1. - 0.5 * (p - p_) / (p + B_));
        } else {
            return 1. / (std::pow(p / p_, 0.5 * (gamma_ + 1.) / gamma_) * (rho_ * a_));
        }
    }
};

struct StarRegion {
    double u;
    double p;
};

inline StarRegion solveStar(const vec3 &left, const vec3 &right, const double gamma) {
    const double tol = 1e-6;

    const PressureFunction fLeft(left(0), left(2), gamma);
    const PressureFunction fRight(right(0), right(2), gamma);
    const auto f    = [&](double p) { return fLeft(p) + fRight(p) + right(1) - left(1); };
    const auto df   = [&](double p) { return fLeft.df(p) + fRight.df(p); };
    const auto iter = [&](double p) { return p - f(p) / df(p); };

    double p0 = 0.5 * (left(2) + right(2));
    double p  = iter(p0);
    if (p < 0) {
        p = tol;
    }
    double error = 2. * std::abs((p - p0) / (p + p0));
    while (error > tol) {
        p0    = std::exchange(p, iter(p));
        error = 2. * std::abs((p - p0) / (p + p0));
    }

    const double u = 0.5 * (left(1) + right(1) + fRight(p) - fLeft(p));

    return {u, p};
}

}  // namespace details

class AnalyticalSolution {
private:
    double x0_;
    vec3 left_;
    vec3 right_;
    double gamma_;

    double aLeft;
    double aRight;

    double pStar;
    double uStar;
    double rhoStarLeft_;
    double rhoStarRight_;

    bool leftShock;
    double leftHeadVelocity;
    double leftTailVelocity;

    bool rightShock;
    double rightHeadVelocity;
    double rightTailVelocity;

public:
    AnalyticalSolution(const double x0, const vec3 &left, const vec3 &right, const double gamma)
        : x0_{x0},
          left_{left},
          right_{right},
          gamma_{gamma},
          aLeft{std::sqrt(gamma * left(2) / left(0))},
          aRight{std::sqrt(gamma * right(2) / right(0))} {
        const auto starSolution = details::solveStar(left_, right_, gamma_);

        pStar = starSolution.p;
        uStar = starSolution.u;

        leftShock = pStar > left(2);
        if (leftShock) {
            leftHeadVelocity =
                left_(1) - aLeft * std::sqrt(0.5 * ((gamma + 1.) / gamma * pStar / left(2) + (gamma - 1.) / gamma));
            leftTailVelocity = leftHeadVelocity;
            rhoStarLeft_     = left(0) * ((pStar / left(2) + (gamma - 1.) / (gamma + 1.)) /
                                      ((gamma - 1.) / (gamma + 1.) * pStar / left(2) + 1.));
        } else {
            const double aStar = aLeft * std::pow(pStar / left(2), 0.5 * (gamma - 1.) / gamma);
            leftHeadVelocity   = left(1) - aLeft;
            leftTailVelocity   = uStar - aStar;
            rhoStarLeft_       = left(0) * std::pow(pStar / left(2), 1. / gamma);
        }

        rightShock = pStar > right(2);
        if (rightShock) {
            rightHeadVelocity =
                right(1) + aRight * std::sqrt(0.5 * ((gamma + 1.) / gamma * pStar / right(2) + (gamma - 1.) / gamma));
            rightTailVelocity = rightHeadVelocity;
            rhoStarRight_     = right(0) * ((pStar / right(2) + (gamma - 1.) / (gamma + 1.)) /
                                        ((gamma - 1.) / (gamma + 1.) * pStar / right(2) + 1.));
        } else {
            const double aStar = aRight * std::pow(pStar / right(2), 0.5 * (gamma - 1.) / gamma);
            rightHeadVelocity  = right(1) + aRight;
            rightTailVelocity  = uStar + aStar;
            rhoStarRight_      = right(0) * std::pow(pStar / right(2), 1. / gamma);
        }
    }

    vec3 operator()(const double t, double x) const {
        x -= x0_;
        const double s = x / t;
        if (s <= uStar) {
            if (leftShock) {
                if (s <= leftHeadVelocity) {
                    return left_;
                } else {
                    return {rhoStarLeft_, uStar, pStar};
                }
            } else {
                if (s <= leftHeadVelocity) {
                    return left_;
                } else if (s >= leftTailVelocity) {
                    return {rhoStarLeft_, uStar, pStar};
                } else {
                    const double rho =
                        left_(0) * std::pow(2. / (gamma_ + 1.) + (gamma_ - 1.) / (gamma_ + 1.) * (left_(1) - s) / aLeft,
                                            2. / (gamma_ - 1.));
                    const double u = 2. / (gamma_ + 1.) * (aLeft + 0.5 * (gamma_ - 1.) * left_(1) + s);
                    const double p =
                        left_(2) * std::pow(2. / (gamma_ + 1.) + (gamma_ - 1.) / (gamma_ + 1.) * (left_(1) - s) / aLeft,
                                            2. * gamma_ / (gamma_ - 1.));
                    return {rho, u, p};
                }
            }
        } else {
            if (rightShock) {
                if (s >= rightHeadVelocity) {
                    return right_;
                } else {
                    return {rhoStarRight_, uStar, pStar};
                }
            } else {
                if (s >= rightHeadVelocity) {
                    return right_;
                } else if (s <= rightTailVelocity) {
                    return {rhoStarRight_, uStar, pStar};
                } else {
                    const double rho = right_(0) * std::pow(2. / (gamma_ + 1.) - (gamma_ - 1.) / (gamma_ + 1.) *
                                                                                     (right_(1) - s) / aRight,
                                                            2. / (gamma_ - 1.));
                    const double u   = 2. / (gamma_ + 1.) * (-aRight + 0.5 * (gamma_ - 1.) * right_(1) + s);
                    const double p   = right_(2) * std::pow(2. / (gamma_ + 1.) -
                                                                (gamma_ - 1.) / (gamma_ + 1.) * (right_(1) - s) / aRight,
                                                            2. * gamma_ / (gamma_ - 1.));
                    return {rho, u, p};
                }
            }
        }
    }
};

}  // namespace riemann_solvers
