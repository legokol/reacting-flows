#include <filesystem>
#include <fstream>
#include <source_location>

#include <riemann_solvers/Solve.hpp>
#include <riemann_solvers/flux/AUSM.hpp>
#include <riemann_solvers/flux/Godunov.hpp>
#include <riemann_solvers/flux/HLL.hpp>
#include "riemann_solvers/Utils.hpp"

#include "AnalyticalSolution.hpp"

int main() {
    using namespace riemann_solvers;

    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("simulations");
    const auto resPath         = std::filesystem::path{filePath.substr(0, pos)}.make_preferred() / "res";

    const double L      = 1.;
    const std::size_t n = 100;
    const double dx     = L / static_cast<double>(n);

    const double CFL = 0.9;

    const double gamma = 1.4;

    const std::vector<double> T{0.2, 0.2, 0.025, 0.025, 0.025};
    const std::vector<vec3> left{
        {1.0, 0.75, 1.0}, {1.0, -2.0, 0.4}, {1.0, 0.0, 1000.0}, {5.99924, 19.5975, 460.894}, {1.0, -19.59745, 1000.0}};
    const std::vector<vec3> right{
        {0.125, 0.0, 0.1}, {1.0, 2.0, 0.4}, {1.0, 0.0, 0.01}, {5.99242, -6.19633, 46.0950}, {1.0, -19.59745, 0.01}};

    const auto sim = [&](const std::size_t i, const std::string& name, auto flux) {
        const auto path = resPath / std::format("{}_{}", name, i);

        std::vector<vec3> initial(n);
        for (std::size_t j = 0; j < n; ++j) {
            if ((static_cast<double>(j) + 0.5) * dx < 0.5 * L) {
                initial[j] = left[i];
            } else {
                initial[j] = right[i];
            }
        }

        const auto [time, unused] = solve(initial, {path, CFL, T[i], dx, gamma}, flux);
        AnalyticalSolution analytical(0.5 * L, left[i], right[i], gamma);
        std::ofstream rhoWriter(path / "rho_a");
        std::ofstream uWriter(path / "u_a");
        std::ofstream pWriter(path / "p_a");
        for (auto t : time) {
            for (std::size_t j = 0; j < n; ++j) {
                const double x = (static_cast<double>(j) + 0.5) * dx;
                const vec3 u   = analytical(t, x);
                rhoWriter << u(0) << ' ';
                uWriter << u(1) << ' ';
                pWriter << u(2) << ' ';
            }
            rhoWriter << '\n';
            uWriter << '\n';
            pWriter << '\n';
        }

        std::ofstream tWriter(path / "t");
        std::ranges::copy(time, std::ostream_iterator<double>(tWriter, " "));
    };

    for (std::size_t i = 0; i < left.size(); ++i) {
        sim(i, "godunov", godunovFlux);
        sim(i, "hll", HLL);
        sim(i, "hlls", HLLC);
        sim(i, "ausm", AUSM);
    }

    return 0;
}