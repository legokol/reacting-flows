#pragma once

#include "riemann_solvers/Utils.hpp"

namespace riemann_solvers {

vec3 AUSM(vec3 left, vec3 right, const double gamma);

}  // namespace riemann_solvers
