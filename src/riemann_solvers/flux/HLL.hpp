#pragma once

#include "riemann_solvers/Utils.hpp"

namespace riemann_solvers {

vec3 HLL(const vec3& left, const vec3& right, double gamma);
vec3 HLLC(const vec3& left, const vec3& right, double gamma);

vec4 HLLC2D(const vec4& left, const vec4& right, double gamma);

}  // namespace riemann_solvers
