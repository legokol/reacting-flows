#include "HLL.hpp"
#include "riemann_solvers/Utils.hpp"

namespace riemann_solvers {

namespace {

struct WaveSpeed {
    double left;
    double right;
};

WaveSpeed approximateWaveSpeeds(const vec3& left, const vec3& right, const double gamma) {
    const double rho    = 0.5 * (left(0) + right(0));
    const double aLeft  = std::sqrt(gamma * left(2) / left(0));
    const double aRight = std::sqrt(gamma * right(2) / right(0));
    const double a      = 0.5 * (aLeft + aRight);
    const double p      = std::max(0., 0.5 * (left(2) + right(2) - rho * a * (right(1) - left(1))));

    const auto q = [gamma, p](double x) {
        if (p <= x) {
            return 1.;
        } else {
            return std::sqrt(1. + 0.5 * (gamma + 1.) / gamma * (p / x - 1.));
        }
    };

    const double sLeft  = left(1) - q(left(2)) * aLeft;
    const double sRight = right(1) + q(right(2)) * aRight;
    return {sLeft, sRight};
}

}  // namespace

vec3 HLL(const vec3& left, const vec3& right, const double gamma) {
    const auto [sLeft, sRight] =
        approximateWaveSpeeds(conservedToPrimitive(left, gamma), conservedToPrimitive(right, gamma), gamma);
    if (sLeft >= 0) {
        return conservedFlux(left, gamma);
    } else if (sRight <= 0) {
        return conservedFlux(right, gamma);
    } else {
        const vec3 leftFlux  = conservedFlux(left, gamma);
        const vec3 rightFlux = conservedFlux(right, gamma);
        return (sRight * leftFlux - sLeft * rightFlux + sLeft * sRight * (right - left)) / (sRight - sLeft);
    }
}

vec3 HLLC(const vec3& left, const vec3& right, const double gamma) {
    const vec3 leftP           = conservedToPrimitive(left, gamma);
    const vec3 rightP          = conservedToPrimitive(right, gamma);
    const auto [sLeft, sRight] = approximateWaveSpeeds(leftP, rightP, gamma);
    const double sMiddle       = (rightP(2) - leftP(2) + leftP(0) * leftP(1) * (sLeft - leftP(1)) -
                            rightP(0) * rightP(1) * (sRight - rightP(1))) /
                           (leftP(0) * (sLeft - leftP(1)) - rightP(0) * (sRight - rightP(1)));
    const vec3 D{0, 1., sMiddle};

    if (sLeft >= 0) {
        return conservedFlux(left, gamma);
    } else if (sRight <= 0) {
        return conservedFlux(right, gamma);
    } else if (0 < sMiddle) {
        return (sMiddle * (sLeft * left - primitiveFlux(leftP, gamma)) +
                sLeft * (leftP(2) + leftP(0) * (sLeft - leftP(1)) * (sMiddle - leftP(1))) * D) /
               (sLeft - sMiddle);
    } else {
        return (sMiddle * (sRight * right - primitiveFlux(rightP, gamma)) +
                sRight * (rightP(2) + rightP(0) * (sRight - rightP(1)) * (sMiddle - rightP(1))) * D) /
               (sRight - sMiddle);
    }
}

vec4 HLLC2D(const vec4& left, const vec4& right, const double gamma) {
    const vec4 leftP  = conservedToPrimitive(left, gamma);
    const vec4 rightP = conservedToPrimitive(right, gamma);
    const auto [sLeft, sRight] =
        approximateWaveSpeeds({leftP(0), leftP(1), leftP(3)}, {rightP(0), rightP(1), rightP(3)}, gamma);
    const double sMiddle = (rightP(3) - leftP(3) + leftP(0) * leftP(1) * (sLeft - leftP(1)) -
                            rightP(0) * rightP(1) * (sRight - rightP(1))) /
                           (leftP(0) * (sLeft - leftP(1)) - rightP(0) * (sRight - rightP(1)));
    const vec4 D{0, 1, 0, sMiddle};

    if (sLeft >= 0) {
        return conservedFlux(left, gamma);
    } else if (sRight <= 0) {
        return conservedFlux(right, gamma);
    } else if (0 < sMiddle) {
        return (sMiddle * (sLeft * left - primitiveFlux(leftP, gamma)) +
                sLeft * (leftP(3) + leftP(0) * (sLeft - leftP(1)) * (sMiddle - leftP(1))) * D) /
               (sLeft - sMiddle);
    } else {
        return (sMiddle * (sRight * right - primitiveFlux(rightP, gamma)) +
                sRight * (rightP(3) + rightP(0) * (sRight - rightP(1)) * (sMiddle - rightP(1))) * D) /
               (sRight - sMiddle);
    }
}

}  // namespace riemann_solvers