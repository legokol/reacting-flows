#include <iostream>
#include <utility>

#include "riemann_solvers/Utils.hpp"

#include "Godunov.hpp"

namespace riemann_solvers {

namespace {

class PressureFunction {
private:
    double rho_;
    double p_;
    double gamma_;
    double a_;
    double A_;
    double B_;

public:
    PressureFunction(double rho, double p, double gamma)
        : rho_{rho},
          p_{p},
          gamma_{gamma},
          a_{std::sqrt(gamma * p / rho)},
          A_{2. / ((gamma + 1.) * rho)},
          B_{(gamma - 1.) / (gamma + 1.) * p} {}

    double operator()(double p) const {
        if (p > p_) {
            return (p - p_) * std::sqrt(A_ / (p + B_));
        } else {
            return 2. * a_ / (gamma_ - 1.) * (std::pow(p / p_, 0.5 * (gamma_ - 1.) / gamma_) - 1.);
        }
    }

    double df(double p) const {
        if (p > p_) {
            return std::sqrt(A_ / (p + B_)) * (1. - 0.5 * (p - p_) / (p + B_));
        } else {
            return 1. / (std::pow(p / p_, 0.5 * (gamma_ + 1.) / gamma_) * (rho_ * a_));
        }
    }
};

struct StarRegion {
    double u;
    double p;
};

StarRegion solveStar(const vec3 &left, const vec3 &right, const double gamma) {
    const double tol = 1e-6;

    const PressureFunction fLeft(left(0), left(2), gamma);
    const PressureFunction fRight(right(0), right(2), gamma);
    const auto f    = [&](double p) { return fLeft(p) + fRight(p) + right(1) - left(1); };
    const auto df   = [&](double p) { return fLeft.df(p) + fRight.df(p); };
    const auto iter = [&](double p) { return p - f(p) / df(p); };

    double p0 = 0.5 * (left(2) + right(2));
    double p  = iter(p0);
    if (p < 0) {
        p = tol;
    }
    double error = 2. * std::abs((p - p0) / (p + p0));
    while (error > tol) {
        p0    = std::exchange(p, iter(p));
        error = 2. * std::abs((p - p0) / (p + p0));
    }

    const double u = 0.5 * (left(1) + right(1) + fRight(p) - fLeft(p));

    return {u, p};
}

vec3 leftSolution(const vec3 &left, const double uStar, const double pStar, const double gamma) {
    const double aLeft = std::sqrt(gamma * left(2) / left(0));
    if (pStar > left(2)) {
        const double shockVelocity =
            left(1) - aLeft * std::sqrt(0.5 * ((gamma + 1.) / gamma * pStar / left(2) + (gamma - 1.) / gamma));
        if (shockVelocity > 0) {
            return left;
        } else {
            const double rhoStar = left(0) * ((pStar / left(2) + (gamma - 1.) / (gamma + 1.)) /
                                              ((gamma - 1.) / (gamma + 1.) * pStar / left(2) + 1.));
            return {rhoStar, uStar, pStar};
        }
    }
    const double aStar = aLeft * std::pow(pStar / left(2), 0.5 * (gamma - 1.) / gamma);

    const double headVelocity = left(1) - aLeft;
    const double tailVelocity = uStar - aStar;
    if (headVelocity > 0) {
        return left;
    } else if (tailVelocity < 0) {
        const double rhoStar = left(0) * std::pow(pStar / left(2), 1. / gamma);
        return {rhoStar, uStar, pStar};
    } else {
        const double rho =
            left(0) * std::pow(2. / (gamma + 1.) + (gamma - 1.) / (gamma + 1.) * left(1) / aLeft, 2. / (gamma - 1.));
        const double u = 2. / (gamma + 1.) * (aLeft + 0.5 * (gamma - 1.) * left(1));
        const double p = left(2) * std::pow(2. / (gamma + 1.) + (gamma - 1.) / (gamma + 1.) * left(1) / aLeft,
                                            2. * gamma / (gamma - 1.));
        return {rho, u, p};
    }
}

vec3 rightSolution(const vec3 &right, const double uStar, const double pStar, const double gamma) {
    const double aRight = std::sqrt(gamma * right(2) / right(0));
    if (pStar > right(2)) {
        const double shockVelocity =
            right(1) + aRight * std::sqrt(0.5 * ((gamma + 1.) / gamma * pStar / right(2) + (gamma - 1.) / gamma));
        if (shockVelocity < 0) {
            return right;
        } else {
            const double rhoStar = right(0) * ((pStar / right(2) + (gamma - 1.) / (gamma + 1.)) /
                                               ((gamma - 1.) / (gamma + 1.) * pStar / right(2) + 1.));
            return {rhoStar, uStar, pStar};
        }
    }
    const double aStar = aRight * std::pow(pStar / right(2), 0.5 * (gamma - 1.) / gamma);

    const double headVelocity = right(1) + aRight;
    const double tailVelocity = uStar + aStar;
    if (headVelocity < 0) {
        return right;
    } else if (tailVelocity > 0) {
        const double rhoStar = right(0) * std::pow(pStar / right(2), 1. / gamma);
        return {rhoStar, uStar, pStar};
    } else {
        const double rho =
            right(0) * std::pow(2. / (gamma + 1.) - (gamma - 1.) / (gamma + 1.) * right(1) / aRight, 2. / (gamma - 1.));
        const double u = 2. / (gamma + 1.) * (-aRight + 0.5 * (gamma - 1.) * right(1));
        const double p = right(2) * std::pow(2. / (gamma + 1.) - (gamma - 1.) / (gamma + 1.) * right(1) / aRight,
                                             2. * gamma / (gamma - 1.));
        return {rho, u, p};
    }
}

}  // namespace

vec3 godunovFlux(vec3 left, vec3 right, const double gamma) {
    if (left == right) {
        return conservedFlux(left, gamma);
    }

    left  = conservedToPrimitive(left, gamma);
    right = conservedToPrimitive(right, gamma);

    const auto [uStar, pStar] = solveStar(left, right, gamma);

    if (uStar >= 0) {
        const vec3 solution = leftSolution(left, uStar, pStar, gamma);
        return primitiveFlux(solution, gamma);
    }
    const vec3 solution = rightSolution(right, uStar, pStar, gamma);
    return primitiveFlux(solution, gamma);
}

vec4 godunovFlux2D(vec4 left, vec4 right, const double gamma) {
    if (left == right) {
        return conservedFlux(left, gamma);
    }

    left  = conservedToPrimitive(left, gamma);
    right = conservedToPrimitive(right, gamma);

    const vec3 left1d  = {left(0), left(1), left(3)};
    const vec3 right1d = {right(0), right(1), right(3)};

    const auto [uStar, pStar] = solveStar(left1d, right1d, gamma);

    if (uStar >= 0) {
        const vec3 solution = leftSolution(left1d, uStar, pStar, gamma);
        return primitiveFlux(vec4{solution(0), solution(1), left(2), solution(2)}, gamma);
    }
    const vec3 solution = rightSolution(right1d, uStar, pStar, gamma);
    return primitiveFlux(vec4{solution(0), solution(1), right(2), solution(2)}, gamma);
}

}  // namespace riemann_solvers