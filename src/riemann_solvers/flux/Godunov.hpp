#pragma once

#include "riemann_solvers/Utils.hpp"

namespace riemann_solvers {

vec3 godunovFlux(vec3 left, vec3 right, double gamma);
vec4 godunovFlux2D(vec4 left, vec4 right, double gamma);

}  // namespace riemann_solvers
