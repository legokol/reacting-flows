#include "AUSM.hpp"

namespace riemann_solvers {

namespace {

double MPLus(const double M) {
    if (std::abs(M) > 1.) {
        return 0.5 * (M + std::abs(M));
    } else {
        return 0.25 * (M + 1.) * (M + 1.);
    }
}

double MMinus(const double M) {
    if (std::abs(M) > 1.) {
        return 0.5 * (M - std::abs(M));
    } else {
        return -0.25 * (M - 1.) * (M - 1.);
    }
}

double pPLus(const double p, const double M) {
    if (std::abs(M) > 1.) {
        return 0.5 * p * (M + std::abs(M)) / M;
    } else {
        return 0.5 * p * (1. + M);
    }
}

double pMinus(const double p, const double M) {
    if (std::abs(M) > 1.) {
        return 0.5 * p * (M - std::abs(M)) / M;
    } else {
        return 0.5 * p * (1. - M);
    }
}

vec3 AUSMFlux(const vec3 &v, const double a, const double gamma) {
    return {v(0) * a, v(0) * v(1) * a, v(0) * a * (0.5 * v(1) * v(1) + a * a / (gamma - 1.))};
}

}  // namespace

vec3 AUSM(vec3 left, vec3 right, const double gamma) {
    left                = conservedToPrimitive(left, gamma);
    right               = conservedToPrimitive(right, gamma);
    const double aLeft  = std::sqrt(gamma * left(2) / left(0));
    const double aRight = std::sqrt(gamma * right(2) / right(0));
    const double MLeft  = left(1) / aLeft;
    const double MRight = right(1) / aRight;

    const double M = MPLus(MLeft) + MMinus(MRight);
    const double p = pPLus(left(2), MLeft) + pMinus(right(2), MRight);

    if (M >= 0) {
        return M * AUSMFlux(left, aLeft, gamma) + vec3{0, p, 0};
    } else {
        return M * AUSMFlux(right, aRight, gamma) + vec3{0, p, 0};
    }
}

}  // namespace riemann_solvers