#include <format>
#include <fstream>

#include "Solve.hpp"

namespace riemann_solvers {

void writePrimitive(const std::filesystem::path &path, const std::vector<vec3> &u) {
    const auto writeParameters = std::ios::app;

    std::fstream rhoWriter(path / "rho", writeParameters);
    std::fstream vWriter(path / "u", writeParameters);
    std::fstream pWriter(path / "p", writeParameters);

    for (const auto &v : u) {
        rhoWriter << v.x() << ' ';
        vWriter << v.y() << ' ';
        pWriter << v.z() << ' ';
    }
    rhoWriter << '\n';
    vWriter << '\n';
    pWriter << '\n';
}

void writePrimitive(const std::filesystem::path &path, const std::vector<vec4> &u, std::size_t idx) {
    std::vector<double> rho(u.size());
    std::vector<double> vx(u.size());
    std::vector<double> vy(u.size());
    std::vector<double> p(u.size());

    for (std::size_t i = 0; i < u.size(); ++i) {
        rho[i] = u[i].x();
        vx[i]  = u[i].y();
        vy[i]  = u[i].z();
        p[i]   = u[i].w();
    }

    constexpr auto writeParameters = std::ios::out | std::ios::binary;
    std::fstream writer;

    writer.open(path / std::format("rho{}.raw", idx), writeParameters);
    writer.write(reinterpret_cast<const char *>(rho.data()), static_cast<std::streamsize>(rho.size() * sizeof(double)));
    writer.close();

    writer.open(path / std::format("u{}.raw", idx), writeParameters);
    writer.write(reinterpret_cast<const char *>(vx.data()), static_cast<std::streamsize>(vx.size() * sizeof(double)));
    writer.close();

    writer.open(path / std::format("v{}.raw", idx), writeParameters);
    writer.write(reinterpret_cast<const char *>(vy.data()), static_cast<std::streamsize>(vy.size() * sizeof(double)));
    writer.close();

    writer.open(path / std::format("p{}.raw", idx), writeParameters);
    writer.write(reinterpret_cast<const char *>(p.data()), static_cast<std::streamsize>(p.size() * sizeof(double)));
    writer.close();
}

}  // namespace riemann_solvers