#pragma once

namespace riemann_solvers::constant {

inline constexpr double gamma_air = 1.4;

inline constexpr double atmosphere = 101325.;

}  // namespace riemann_solvers::constant
