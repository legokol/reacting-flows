#pragma once

#include <Eigen/Dense>

namespace riemann_solvers {

using vec3 = Eigen::Vector3d;
using vec4 = Eigen::Vector4d;
using vec5 = Eigen::Vector<double, 5>;

// 1D
inline vec3 conservedToPrimitive(const vec3 &v, const double gamma) {
    return {v(0), v(1) / v(0), (gamma - 1.) * (v(2) - 0.5 * v(1) * v(1) / v(0))};
}

inline vec3 primitiveToConserved(const vec3 &v, const double gamma) {
    return {v(0), v(0) * v(1), 0.5 * v(0) * v(1) * v(1) + v(2) / (gamma - 1.)};
}

inline vec3 primitiveFlux(const vec3 &v, const double gamma) {
    const double rhoU = v(0) * v(1);
    return {rhoU, rhoU * v(1) + v(2), v(1) * (0.5 * rhoU * v(1) + gamma / (gamma - 1.) * v(2))};
}

inline vec3 conservedFlux(const vec3 &v, const double gamma) {
    const double u = v(1) / v(0);
    return {v(1), 0.5 * (3. - gamma) * v(1) * u + (gamma - 1.) * v(2),
            gamma * u * v(2) - 0.5 * (gamma - 1.) * u * u * v(1)};
}

// 2D
inline vec4 conservedToPrimitive(const vec4 &v, const double gamma) {
    return {v(0), v(1) / v(0), v(2) / v(0), (gamma - 1.) * (v(3) - 0.5 * (v(1) * v(1) + v(2) * v(2)) / v(0))};
}

inline vec4 primitiveToConserved(const vec4 &v, const double gamma) {
    return {v(0), v(0) * v(1), v(0) * v(2), 0.5 * v(0) * (v(1) * v(1) + v(2) * v(2)) + v(3) / (gamma - 1.)};
}

inline vec4 primitiveFlux(const vec4 &v, const double gamma) {
    const double rhoU = v(0) * v(1);
    return {rhoU, rhoU * v(1) + v(3), rhoU * v(2),
            v(1) * (0.5 * v(0) * (v(1) * v(1) + v(2) * v(2)) + gamma / (gamma - 1.) * v(3))};
}

inline vec4 conservedFlux(const vec4 &v, const double gamma) {
    return primitiveFlux(conservedToPrimitive(v, gamma), gamma);
}

// 3D
inline vec5 conservedToPrimitive(const vec5 &v, const double gamma) {
    return {v(0), v(1) / v(0), v(2) / v(0), v(3) / v(0),
            (gamma - 1.) * (v(4) - 0.5 * (v(1) * v(1) + v(2) * v(2) + v(3) * v(3)) / v(0))};
}

inline vec5 primitiveToConserved(const vec5 &v, const double gamma) {
    return {v(0), v(0) * v(1), v(0) * v(2), v(0) * v(3),
            0.5 * v(0) * (v(1) * v(1) + v(2) * v(2) + v(3) * v(3)) + v(4) / (gamma - 1.)};
}

inline vec5 primitiveFlux(const vec5 &v, const double gamma) {
    const double rhoU = v(0) * v(1);
    return {rhoU, rhoU * v(1) + v(4), rhoU * v(2), rhoU * v(3),
            v(1) * (0.5 * v(0) * (v(1) * v(1) + v(2) * v(2) + v(3) * v(3)) + gamma / (gamma - 1.) * v(4))};
}

inline vec5 conservedFlux(const vec5 &v, const double gamma) {
    return primitiveFlux(conservedToPrimitive(v, gamma), gamma);
}

}  // namespace riemann_solvers
