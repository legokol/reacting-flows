#pragma once

#include <filesystem>
#include <ranges>
#include <utility>

#include "riemann_solvers/Utils.hpp"

namespace riemann_solvers {

struct Parameters {
    std::filesystem::path path;
    double CFL;
    double T;
    double dx;
    double gamma;
};

void writePrimitive(const std::filesystem::path& path, const std::vector<vec3>& u);
void writePrimitive(const std::filesystem::path& path, const std::vector<vec4>& u, std::size_t idx);

inline void writeConserved(const std::filesystem::path& path, const std::vector<vec3>& u, const double gamma) {
    std::vector<vec3> p(u.size());
    std::ranges::transform(u, p.begin(), [gamma](const vec3& v) { return conservedToPrimitive(v, gamma); });
    writePrimitive(path, p);
}

inline void writeConserved(const std::filesystem::path& path, const std::vector<vec4>& u, const double gamma,
                           std::size_t idx) {
    std::vector<vec4> p(u.size());
    std::ranges::transform(u, p.begin(), [gamma](const vec4& v) { return conservedToPrimitive(v, gamma); });
    writePrimitive(path, p, idx);
}

inline double vMax(const std::vector<vec3>& u, const double gamma) {
    double result = 0.;
    for (const auto& vec : u) {
        const vec3 v = conservedToPrimitive(vec, gamma);
        result       = std::max(result, std::abs(v(1)) + std::sqrt(gamma * v(2) / v(0)));
    }
    return result;
}

inline double vMax(const std::vector<vec4>& u, const double gamma) {
    double result = 0.;
    for (const auto& vec : u) {
        const vec4 v = conservedToPrimitive(vec, gamma);
        result       = std::max(result, std::hypot(v(1), v(2)) + std::sqrt(gamma * v(3) / v(0)));
    }
    return result;
}

struct ReturnType {
    std::vector<double> timeGrid;
    std::vector<vec3> solution;
};

template <typename Flux>
ReturnType solve(std::vector<vec3> u, const Parameters& parameters, Flux&& flux) {
    const auto [path, CFL, T, dx, gamma] = parameters;
    std::filesystem::remove_all(path);
    std::filesystem::create_directories(path);
    writePrimitive(path, u);
    std::ranges::transform(u, u.begin(), [gamma](const vec3& v) { return primitiveToConserved(v, gamma); });

    double t = 0;
    std::vector<double> timeGrid{0};
    while (t < T) {
        const double dt = CFL * dx / vMax(u, gamma);
        t += dt;
        timeGrid.push_back(t);

        vec3 fluxLeft  = flux(u[0], u[0], gamma);
        vec3 fluxRight = flux(u[0], u[1], gamma);
        u[0] -= (dt / dx) * (fluxRight - fluxLeft);
        for (std::size_t i = 1; i + 1 < u.size(); ++i) {
            fluxLeft = std::exchange(fluxRight, flux(u[i], u[i + 1], gamma));
            u[i] -= (dt / dx) * (fluxRight - fluxLeft);
        }
        fluxLeft = std::exchange(fluxRight, flux(u.back(), u.back(), gamma));
        u.back() -= (dt / dx) * (fluxRight - fluxLeft);

        writeConserved(path, u, gamma);
    }

    std::vector<vec3> result(u.size());
    std::ranges::transform(u, result.begin(), [gamma](const vec3& v) { return conservedToPrimitive(v, gamma); });
    return {timeGrid, result};
}

struct ReturnType2D {
    std::vector<double> timeGrid;
    std::vector<vec4> solution;
};

template <typename Flux>
ReturnType2D solve(std::vector<vec4> u, std::size_t sizeX, std::size_t sizeY, const Parameters& parameters, Flux flux) {
    const auto [path, CFL, T, dx, gamma] = parameters;
    std::filesystem::remove_all(path);
    std::filesystem::create_directories(path);
    writePrimitive(path, u, 0);
    std::ranges::transform(u, u.begin(), [gamma](const vec4& v) -> vec4 { return primitiveToConserved(v, gamma); });

    constexpr auto rotate = [](const vec4& v) -> vec4 { return {v.x(), v.z(), v.y(), v.w()}; };
    const auto index      = [sizeX](std::size_t i, std::size_t j) { return i + j * sizeX; };

    double t = 0;
    std::vector<double> timeGrid{0};
    std::vector<vec4> fluxBottom(sizeX);
    std::size_t idx = 0;
    while (t < T) {
        ++idx;
        const double dt = CFL * dx / vMax(u, gamma);
        t += dt;
        timeGrid.push_back(t);

        for (std::size_t i = 0; i < sizeX; ++i) {
            fluxBottom[i] = rotate(flux(rotate(u[index(i, 0)]), rotate(u[index(i, 0)]), gamma));
        }
        for (std::size_t j = 0; j + 1 < sizeY; ++j) {
            vec4 fluxLeft;
            vec4 fluxRight = flux(u[index(0, j)], u[index(0, j)], gamma);
            for (std::size_t i = 0; i + 1 < sizeX; ++i) {
                fluxLeft     = std::exchange(fluxRight, flux(u[index(i, j)], u[index(i + 1, j)], gamma));
                vec4 fluxTop = rotate(flux(rotate(u[index(i, j)]), rotate(u[index(i, j + 1)]), gamma));
                u[index(i, j)] -= (dt / dx) * (fluxRight - fluxLeft + fluxTop - fluxBottom[i]);
                fluxBottom[i] = fluxTop;
            }
            const std::size_t i = sizeX - 1;
            fluxLeft            = std::exchange(fluxRight, flux(u[index(i, j)], u[index(i, j)], gamma));
            vec4 fluxTop        = rotate(flux(rotate(u[index(i, j)]), rotate(u[index(i, j + 1)]), gamma));
            u[index(i, j)] -= (dt / dx) * (fluxRight - fluxLeft + fluxTop - fluxBottom[i]);
            fluxBottom[i] = fluxTop;
        }

        {
            const std::size_t j = sizeY - 1;
            vec4 fluxLeft;
            vec4 fluxRight = flux(u[index(0, j)], u[index(0, j)], gamma);
            for (std::size_t i = 0; i + 1 < sizeX; ++i) {
                fluxLeft     = std::exchange(fluxRight, flux(u[index(i, j)], u[index(i + 1, j)], gamma));
                vec4 fluxTop = rotate(flux(rotate(u[index(i, j)]), rotate(u[index(i, j)]), gamma));
                u[index(i, j)] -= (dt / dx) * (fluxRight - fluxLeft + fluxTop - fluxBottom[i]);
            }
            const std::size_t i = sizeX - 1;
            fluxLeft            = std::exchange(fluxRight, flux(u[index(i, j)], u[index(i, j)], gamma));
            vec4 fluxTop        = rotate(flux(rotate(u[index(i, j)]), rotate(u[index(i, j)]), gamma));
            u[index(i, j)] -= (dt / dx) * (fluxRight - fluxLeft + fluxTop - fluxBottom[i]);
        }

        writeConserved(path, u, gamma, idx);
    }

    std::vector<vec4> result(u.size());
    std::ranges::transform(u, result.begin(), [gamma](const vec4& v) { return conservedToPrimitive(v, gamma); });
    return {timeGrid, result};
};

}  // namespace riemann_solvers
